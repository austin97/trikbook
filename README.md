# Trikbook©

Plans for app:

- Create a dictionary of flatground skateboarding tricks with a description and possible gif of trick
- Creat a profile where you can add tricks from the dictionary to your profile to say you've done them
- each trick would have a level of proficiency in the trick (i.e done it once, can do it most of the time, or 100% down)
- users would be able to upload video proof of doing the trick (to show your friends who don't believe you)
- eventually be able to look at other users profiles and see their tricks

Possible ideas for future implementation

- create points where a trick might be worth more points than others
- highscores of who has the most points
